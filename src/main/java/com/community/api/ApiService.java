package com.community.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 *
 * @author Natsuki
 */
public class ApiService {
    private static final String BASE_URL = "http://localhost/belajar-spring/";
    private static Retrofit retrofit;

    public static ProdukRestClient endPoint(){
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        return retrofit.create(ProdukRestClient.class);
    }
}
