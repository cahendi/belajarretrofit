package com.community.api;

import com.community.request.Produk;
import com.community.request.ResponseData;
import java.util.List;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

/**
 *
 * @author Natsuki
 */
public interface ProdukRestClient {
    
    @GET("master-produk/list")
    public Call<ResponseData<List<Produk>>> getListProduk();

    @GET("master-produk/{id}")
    public Call<ResponseData<Produk>> getProdukByID(@Path("id") long id);

    @POST("master-produk/create")
    public Call<ResponseData<Produk>> createProduk(@Body Produk produk);
    
    @PUT("master-produk/update/{id}")
    public Call<ResponseData<Produk>> updateProduk(@Path("id") long id, @Body Produk produk);
    
    @DELETE("master-produk/delete/{id}")
    public Call<ResponseData<Produk>> deleteProduk(@Path("id") long id);
}
