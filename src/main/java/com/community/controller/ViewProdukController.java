package com.community.controller;

import com.community.api.ApiService;
import com.community.request.Produk;
import com.community.request.ResponseData;
import com.community.view.DialogProduk;
import com.community.view.ViewProduk;
import javax.swing.JOptionPane;
import static javax.swing.JOptionPane.INFORMATION_MESSAGE;
import static javax.swing.JOptionPane.ERROR;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 *
 * @author Natsuki
 */
public class ViewProdukController {

    private final ViewProduk view;

    public ViewProdukController(ViewProduk view) {
        this.view = view;
    }

    public void tambahData() {
        DialogProduk dialog = new DialogProduk(view, Produk.builder().build());
        dialog.setVisible(true);
    }

    public void updateData(int row) {
        Produk produk = view.getModel().getDataProduk(row);
        DialogProduk dialog = new DialogProduk(view, produk);
        dialog.setVisible(true);
    }

    public void deleteData(int row) {
        Produk produk = view.getModel().getDataProduk(row);

        Call<ResponseData<Produk>> call = ApiService.endPoint().deleteProduk(produk.getId());
        call.enqueue(new Callback<ResponseData<Produk>>() {
            @Override
            public void onResponse(Call<ResponseData<Produk>> call, Response<ResponseData<Produk>> response) {
                if (response.isSuccessful()) {
                    view.getModel().removeRow(produk);
                    view.getModel().fireTableDataChanged();
                    JOptionPane.showMessageDialog(null, "Data Produk Berhasil di Hapus", "Sukses", INFORMATION_MESSAGE);
                } else {
                    JOptionPane.showMessageDialog(null, "Data Produk Gagal di Hapus", "Error", ERROR);
                }
            }

            @Override
            public void onFailure(Call<ResponseData<Produk>> call, Throwable thrwbl) {
                JOptionPane.showMessageDialog(null, "Tidak Dapat Terhubung Ke Server", "Error", ERROR);
            }

        });
    }
    
}