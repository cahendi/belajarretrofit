package com.community.controller;

import com.community.api.ApiService;
import com.community.request.Produk;
import com.community.request.ResponseData;
import com.community.view.DialogProduk;
import static java.awt.image.ImageObserver.ERROR;
import javax.swing.JOptionPane;
import static javax.swing.JOptionPane.INFORMATION_MESSAGE;
import static javax.swing.JOptionPane.WARNING_MESSAGE;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 *
 * @author Akira
 */
public class DialogProdukController {

    private final DialogProduk dialog;

    public DialogProdukController(DialogProduk dialog) {
        this.dialog = dialog;
    }
    
    public void getData(){
        dialog.getFieldKodeProduk().setText(dialog.getProduk().getKodeProduk());
        dialog.getFieldNamaProduk().setText(dialog.getProduk().getNamaProduk());
    }

    public void tambahData() {
        if (validasi()) {
            if (dialog.getProduk().getId() > 0) {
                updateData(dialog.getProduk().getId());
            } else {
                simpanData();
            }
            dialog.dispose();
        }
    }

    private boolean validasi() {
        boolean result = true;

        if (dialog.getFieldKodeProduk().getText().trim().equals("")
                || dialog.getFieldNamaProduk().getText().trim().equals("")) {
            result = false;
        }

        if (!result) {
            JOptionPane.showMessageDialog(null, "Kode dan Nama Produk Tidak Boleh Kosong", "Perhatian", WARNING_MESSAGE);
        }

        return result;
    }

    private void simpanData() {
        Call<ResponseData<Produk>> call = ApiService.endPoint()
                .createProduk(Produk.builder()
                        .kodeProduk(dialog.getFieldKodeProduk().getText())
                        .namaProduk(dialog.getFieldNamaProduk().getText())
                        .build());
        call.enqueue(new Callback<ResponseData<Produk>>() {
            @Override
            public void onResponse(Call<ResponseData<Produk>> call, Response<ResponseData<Produk>> response) {
                if (response.isSuccessful()) {
                    dialog.getView().getModel().addRow(response.body().getData());
                    dialog.getView().getModel().fireTableDataChanged();

                    JOptionPane.showMessageDialog(null, "Data Produk Berhasil di Simpan", "Sukses", INFORMATION_MESSAGE);
                } else {
                    JOptionPane.showMessageDialog(null, "Data Produk Gagal di Simpan", "Error", ERROR);
                }
            }

            @Override
            public void onFailure(Call<ResponseData<Produk>> call, Throwable thrwbl) {
                JOptionPane.showMessageDialog(null, "Tidak Dapat Terhubung Ke Server", "Error", ERROR);
            }

        });
    }

    public void updateData(long idProduk) {
        Call<ResponseData<Produk>> call = ApiService.endPoint()
                .updateProduk(idProduk, Produk.builder()
                        .kodeProduk(dialog.getFieldKodeProduk().getText())
                        .namaProduk(dialog.getFieldNamaProduk().getText())
                        .build());

        call.enqueue(new Callback<ResponseData<Produk>>() {
            @Override
            public void onResponse(Call<ResponseData<Produk>> call, Response<ResponseData<Produk>> response) {
                if (response.isSuccessful()) {
                    dialog.getView().getModel().removeRow(dialog.getProduk());
                    dialog.getView().getModel().addRow(response.body().getData());
                    dialog.getView().getModel().fireTableDataChanged();
                    JOptionPane.showMessageDialog(null, "Data Produk Berhasil di Update", "Sukses", INFORMATION_MESSAGE);
                } else {
                    JOptionPane.showMessageDialog(null, "Data Produk Gagal di Update", "Error", ERROR);
                }
            }

            @Override
            public void onFailure(Call<ResponseData<Produk>> call, Throwable thrwbl) {
                JOptionPane.showMessageDialog(null, "Tidak Dapat Terhubung Ke Server", "Error", ERROR);
            }

        });
    }
}
