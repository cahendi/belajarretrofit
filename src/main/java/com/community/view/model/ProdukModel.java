package com.community.view.model;

import com.community.api.ApiService;
import com.community.request.Produk;
import com.community.request.ResponseData;
import java.util.List;
import javax.swing.JOptionPane;
import static javax.swing.JOptionPane.ERROR;
import javax.swing.table.AbstractTableModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 *
 * @author Natsuki
 */
public class ProdukModel extends AbstractTableModel {

    private List<Produk> listProduk;
    
    private final String header[] = new String[]{
        "Kode Produk", "Nama Produk"
    };

    public ProdukModel() {
        getListProduk();
    }

    private void getListProduk() {
        Call<ResponseData<List<Produk>>> call = ApiService.endPoint().getListProduk();
        call.enqueue(new Callback<ResponseData<List<Produk>>>() {
            @Override
            public void onResponse(Call<ResponseData<List<Produk>>> call, Response<ResponseData<List<Produk>>> response) {
                if (response.isSuccessful()) {
                    listProduk = response.body().getData();
                }
            }

            @Override
            public void onFailure(Call<ResponseData<List<Produk>>> call, Throwable thrwbl) {
                JOptionPane.showMessageDialog(null, "Gagal Mengambil Data Produk", "Error", ERROR);
            }

        });
    }

    public Produk getDataProduk(int row) {
        return listProduk.get(row);
    }

    public void addRow(Produk produk) {
        listProduk.add(produk);
    }

    public void removeRow(Produk produk) {
        listProduk.remove(produk);
    }

    public void reloadData() {
        if (!listProduk.isEmpty()) {
            listProduk.clear();
        }
    }

    @Override
    public int getRowCount() {
        if (!listProduk.isEmpty()) {
            return listProduk.size();
        } else {
            return 0;
        }
    }

    @Override
    public int getColumnCount() {
        return header.length;
    }

    @Override
    public String getColumnName(int column) {
        return header[column];
    }

    @Override
    public Object getValueAt(int row, int column) {
        switch (column) {
            case 0:
                return listProduk.get(row).getKodeProduk();
            case 1:
                return listProduk.get(row).getNamaProduk();
            default:
                return null;
        }
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 0:
                return String.class;
            case 1:
                return String.class;
            default:
                return null;
        }
    }
}
