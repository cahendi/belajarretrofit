package com.community.view;

import com.community.controller.ViewProdukController;
import com.community.view.model.ProdukModel;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

/**
 *
 * @author Natsuki
 */
public class ViewProduk extends JFrame implements ActionListener {

    private final ViewProdukController controller;
    private JTable tabel;
    private ProdukModel model;
    private JButton tambah, edit, hapus, batal;

    public ViewProduk() {
        controller = new ViewProdukController(this);
        initGUI();
    }

    private void initGUI() {
        setTitle("Rest Client Retrofit");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setResizable(false);
        setSize(700, 500);
        setLocationRelativeTo(null);

        JPanel panelBtn = new JPanel(new FlowLayout(FlowLayout.LEFT, 10, 10));

        tambah = new JButton("Tambah");
        tambah.setPreferredSize(new Dimension(120, 30));
        tambah.addActionListener(this);
        panelBtn.add(tambah);

        edit = new JButton("Edit");
        edit.setPreferredSize(new Dimension(120, 30));
        edit.addActionListener(this);
        panelBtn.add(edit);

        hapus = new JButton("Hapus");
        hapus.setPreferredSize(new Dimension(120, 30));
        hapus.addActionListener(this);
        panelBtn.add(hapus);

        batal = new JButton("Batal");
        batal.setPreferredSize(new Dimension(120, 30));
        batal.addActionListener(this);
        panelBtn.add(batal);

        getContentPane().add(panelBtn, BorderLayout.NORTH);

        model = new ProdukModel();

        tabel = new JTable(model);
        getContentPane().add(new JScrollPane(tabel));
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == tambah) {
            controller.tambahData();
        } else if (e.getSource() == edit) {
            int row = tabel.getSelectedRow();
            controller.updateData(row);
        } else if (e.getSource() == hapus) {
            int row = tabel.getSelectedRow();
            controller.deleteData(row);
        }
    }

    public ProdukModel getModel() {
        return model;
    }

}
