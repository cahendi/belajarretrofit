package com.community.view;

import com.community.controller.DialogProdukController;
import com.community.request.Produk;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author Akira
 */
public class DialogProduk extends JDialog implements ActionListener{
    
    private final ViewProduk view;
    private final Produk produk;
    private final DialogProdukController controller;
    private JTextField fieldKodeProduk, fieldNamaProduk;
    private JButton simpan, batal;
    
    public DialogProduk(ViewProduk view, Produk produk){
        this.view = view;
        this.produk = produk;
        controller = new DialogProdukController(this);
        initGUI();
        controller.getData();
    }

    private void initGUI() {
        setTitle("Form Produk");
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setSize(300, 300);
        setResizable(false);
        setModal(true);
        setLocationRelativeTo(null);
        
        JPanel panelForm = new JPanel(new FlowLayout(FlowLayout.LEFT, 10, 10));
        
        JLabel labelKodeProduk = new JLabel("Kode Produk");
        labelKodeProduk.setPreferredSize(new Dimension(120, 30));
        panelForm.add(labelKodeProduk);
        
        fieldKodeProduk = new JTextField();
        fieldKodeProduk.setPreferredSize(new Dimension(250, 30));
        panelForm.add(fieldKodeProduk);
        
        JLabel labelNamaProduk = new JLabel("Nama Produk");
        labelNamaProduk.setPreferredSize(new Dimension(120, 30));
        panelForm.add(labelNamaProduk);
        
        fieldNamaProduk = new JTextField();
        fieldNamaProduk.setPreferredSize(new Dimension(250, 30));
        panelForm.add(fieldNamaProduk);
        
        getContentPane().add(panelForm);
        
        JPanel panelBtn = new JPanel(new FlowLayout(FlowLayout.LEFT, 10, 10));

        simpan = new JButton("Simpan");
        simpan.setPreferredSize(new Dimension(120, 30));
        simpan.addActionListener(this);
        panelBtn.add(simpan);

        batal = new JButton("Batal");
        batal.setPreferredSize(new Dimension(120, 30));
        batal.addActionListener(this);
        panelBtn.add(batal);
        
        getContentPane().add(panelBtn, BorderLayout.SOUTH);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource()==simpan){
            controller.tambahData();
        } else if(e.getSource()==batal){
            this.dispose();
        }
    }

    public ViewProduk getView() {
        return view;
    }

    public Produk getProduk() {
        return produk;
    }

    public JTextField getFieldKodeProduk() {
        return fieldKodeProduk;
    }

    public JTextField getFieldNamaProduk() {
        return fieldNamaProduk;
    }
    
}
