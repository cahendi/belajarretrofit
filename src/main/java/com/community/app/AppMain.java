package com.community.app;

import com.community.view.ViewProduk;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.plaf.nimbus.NimbusLookAndFeel;

/**
 *
 * @author Natsuki
 */
public class AppMain {
    public static void main(String[] args) throws UnsupportedLookAndFeelException {
        UIManager.setLookAndFeel(new NimbusLookAndFeel());
        ViewProduk view = new ViewProduk();
        view.setVisible(true);
    }
}
