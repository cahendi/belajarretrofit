package com.community.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 *
 * @author Natsuki
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class Produk {
    private long id;
    private String kodeProduk;
    private String namaProduk;
}
