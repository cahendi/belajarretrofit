package com.community.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 *
 * @author Natsuki
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class ResponseData<T> {
    private int status;
    private String pesan;
    private T data; 
}
